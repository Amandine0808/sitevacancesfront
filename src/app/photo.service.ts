import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private httpCLient: HttpClient) { }
  
  private baseUrl = "http://localhost:9090/photo";

  public getAllVoyages() : Observable<any>{
    console.log("getAllPhotos");
    return this.httpCLient.get(this.baseUrl);
  }

  public savePhoto(photo : any): Observable<any>{
    console.log("save photo");
    return this.httpCLient.post(this.baseUrl, photo);
  }

}
