import { Activite } from './activite';
import { Photo } from './photo';
import { Voyage } from './voyage';

export class JourneeBack {
    idJournee : number ; 
    dateJournee : Date ;
    hebergement : String ;
    indexJournee : number ;
    photo : Photo ;
    voyage : Voyage ;
    activites : Array<Activite>;
}
