import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VoyageBack } from './voyage-back';
import { Photo } from './photo';

@Injectable({
  providedIn: 'root'
})

export class VoyageService {
voyageBack : VoyageBack = new VoyageBack ;


  constructor(private httpCLient: HttpClient) { }
  
  private baseUrl = "http://localhost:9090/voyages";

  public getAllVoyages() : Observable<any>{
    console.log("getAllVoyages");
    //attention : on récupère un voyage avec le format back alors que le front a besoin d'un voyage format "nomPhoto et chemin Photo.
    return this.httpCLient.get(this.baseUrl);
  }

  public saveVoyage(voyage : any): Observable<any>{
    //on récupère un voyage avec le format "nomPhoto et chemin Photo" alors que le back a besoin d'un voyage format back. On corrige cela en faisant une conversion.
    this.voyageBack.photo = new Photo ;
    this.voyageBack.idVoyage = voyage.idVoyage ;
    this.voyageBack.plusieursJours = voyage.plusieursJours ;
    this.voyageBack.dateDebut = voyage.dateDebut ;
    this.voyageBack.dateFin = voyage.dateFin ;
    this.voyageBack.lieu = voyage.lieu ;
    this.voyageBack.journees = voyage.journees ;
    this.voyageBack.photo.nom = voyage.nomPhotoVoyage ;
    this.voyageBack.photo.chemin = voyage.cheminPhoto ;
    console.log(this.voyageBack);
    return this.httpCLient.post(this.baseUrl, this.voyageBack);
  }

  public deleteVoyage(id : number): Observable<any>{
    return this.httpCLient.delete(this.baseUrl+"/"+id);
  }

  public getVoyage(id : number): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/"+id) ; 
  }

  public getLastVoyage(): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/idMax") ; 
  }

  public getVoyagesOrdreChrono(): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/ordreChrono") ; 
  }
}

