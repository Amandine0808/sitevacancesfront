
import { Voyage } from "./voyage";
import { Activite } from "./activite";

export class Journee {
    idJournee : number ; 
    dateJournee : Date ;
    hebergement : String ;
    indexJournee : number ;
    nomPhotoJournee : String ;
    cheminPhoto : String ;
    voyage : Voyage ;
    activites : Array<Activite>;
}
