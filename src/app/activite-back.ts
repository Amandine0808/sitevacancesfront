import { Journee } from './journee';
import { Photo } from './photo';

export class ActiviteBack {
    idActivite : number ;
    descriptif : String ;
    photo : Photo ;
    journee : Journee ;
}
