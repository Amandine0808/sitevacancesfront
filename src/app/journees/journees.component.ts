import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Journee } from '../journee';
import { Voyage } from '../voyage';
import { Activite } from '../activite';
import { JourneeService } from '../journee.service';
import { VoyageService } from '../voyage.service';
import { ActiviteService } from '../activite.service';
import { ActiviteBack } from '../activite-back';
import { Photo } from '../photo';
import { PhotoService } from '../photo.service';
import { JourneeBack } from '../journee-back';

@Component({
  selector: 'app-journees',
  templateUrl: './journees.component.html',
  styleUrls: ['./journees.component.css']
})

export class JourneesComponent implements OnInit {
  journees: any[];
  journee: Journee = new Journee;
  journee2: any;
  journeeHebergement: Journee = new Journee;
  voyage: Voyage = new Voyage;
  voyages: any[];
  voyageG: Voyage = new Voyage;
  activites: any[];
  activite: Activite = new Activite;
  divHebergement: NodeListOf<HTMLElement>;
  divActivite: NodeListOf<HTMLElement>;
  divPhoto: NodeListOf<HTMLElement>;
  formHebergement: any;
  formActivite: any;
  formPhoto: any;
  journeeBack: JourneeBack = new JourneeBack;
  activiteBack: ActiviteBack;
  photo: Photo = new Photo;
  formJournee: any;
  index: number;

  constructor(private journeeService: JourneeService, private router: Router, private voyageService: VoyageService, private activiteService: ActiviteService, private photoService: PhotoService) { }

  ngOnInit(): void {
    //déclaration des différents champs du formulaire JOurnée
    this.formJournee = new FormGroup({
      idJournee: new FormControl(),
      dateJournee: new FormControl(),
      hebergement: new FormControl(),
      nomPhotoJournee: new FormControl(),
      cheminPhotoJournee: new FormControl(),
    });
    //déclaration des différents champs du formulaire Hébergement
    this.formHebergement = new FormGroup({
      idJournee: new FormControl(),
      dateJournee: new FormControl(),
      hebergement: new FormControl(),
      voyage: new FormControl()
    });
    //déclaration des différents champs du formulaire Activités
    this.formActivite = new FormGroup({
      idActivite: new FormControl(),
      descriptif: new FormControl(),
      nomPhotoActivite: new FormControl(),
      photo: new FormControl(),
      journee: new FormControl()
    });
    //déclaration des différents champs du formulaire Photos
    this.formPhoto = new FormGroup({
      idPhoto: new FormControl(),
      nom: new FormControl(),
      chemin: new FormControl()
    });
    this.keepVoyage(this.chargerJournee.bind(this));
  }

  //récupérer en mémoire le dernier voyage enregistré puis charger les journées correspondantes
  keepVoyage(chargerJournee) {
    console.log("keep voyage");
    this.voyage = JSON.parse(localStorage.getItem('voyageG'));
    chargerJournee();
  }

  //récupérer l'ensemble des journées du voyage en question
  chargerJournee() {
    //console.log("charger journee");
    this.journeeService.getJourneeByVoyage(this.voyage.idVoyage).subscribe(data1 => {
      this.journees = data1
      //console.log("journees : " + JSON.stringify(this.journees));
    });
  }

  //ouvrir le modal qui demande confirmation avant la suppression d'une journée
  openModalJournee(journee) {
    document.getElementById("modalSuppressionJournee").style.display = "block";
    this.journee = journee;
  }

  //fermer le modal
  closeFormJournee() {
    document.getElementById("modalSuppressionJournee").style.display = "none";
  }

  //supprimer l'activité
  deleteJournee() {
    this.journeeService.deleteJournee(this.journee.idJournee).subscribe(() => {
      this.chargerJournee();
    });
    document.getElementById("modalSuppressionJournee").style.display = "none";
  }

  //enregistrer une nouvelle journée (index nécessaire pour faire fonctionner les toggles des journées nouvellement créées sur cette page)
  saveJournee() {
    console.log("save journée");
    console.log("id voyage" + JSON.stringify(this.voyage.idVoyage));
    this.journeeService.getLastJourneeByVoyage(this.voyage.idVoyage).subscribe(data2 => {
      this.journee2 = data2;
      console.log("journee2" + JSON.stringify(this.journee2));
      this.index = this.journee2.indexJournee + 1;
      console.log("index" + JSON.stringify(this.index));
      this.journeeService.saveJournee3(this.formJournee.value, this.voyage, this.index).subscribe(() => {
        this.chargerJournee();
        this.formJournee.controls['idJournee'].setValue("")
        this.formJournee.controls['dateJournee'].setValue("")
        this.formJournee.controls['hebergement'].setValue("")
        this.formJournee.controls['nomPhotoJournee'].setValue("")
        this.formJournee.controls['cheminPhotoJournee'].setValue("")
      });
    })
    this.closeFormAjoutJournee();
  }

  //ouvrir le modal pour créer une journée
  openModalAjoutJournee() {
    console.log("ouvrir modal");
    document.getElementById("modalAjoutJournee").style.display = "block";
  }
  //fermer le modal
  closeFormAjoutJournee() {
    document.getElementById("modalAjoutJournee").style.display = "none";
  }

  /* !!=========================!!
     !!Concernant l'hébergement !!
     !!=========================!! */


  //Afficher la barre d'enregistrement de l'hébergement
  toggleHebergement(journee: any) {
    console.log("toggle" + JSON.stringify(journee));
    this.divHebergement = document.getElementsByName("divHebergement");
    //console.log("idjournee" + JSON.stringify(this.journee.idJournee));
    console.log("attribut divHebergement" + JSON.stringify(this.divHebergement.item(journee.indexJournee).getAttribute("data-name")));
    if (this.divHebergement.item(journee.indexJournee).getAttribute("data-name") === journee.idJournee.toString()) {
      console.log("entree if");
      console.log(this.divHebergement.item(journee.indexJournee).hidden);
      this.divHebergement.item(journee.indexJournee).hidden = !this.divHebergement.item(journee.indexJournee).hidden;
      console.log(this.divHebergement.item(journee.indexJournee).hidden);
    }
  }

  //ouvre le formulaire hébergement
  editJourneeHebergement(journee: any, toggleHebergement) {
    this.journee = journee;
    console.log("editjournee" + JSON.stringify(journee));
    console.log("journee" + JSON.stringify(this.journee));
    console.log("journee voyage" + JSON.stringify(this.journee.voyage));
    toggleHebergement(journee);
  }

  //vide la liste des journées dans voyage puis rempli l'attribut voyage dans la classe journée
  saveHebergement() {
    console.log("save");
    this.voyage.journees = [];
    this.journee.hebergement = this.formHebergement.controls['hebergement'].value;
    this.journee.voyage = this.voyage;
    console.log("voyage " + JSON.stringify(this.voyage));
    console.log("journee " + JSON.stringify(this.journee));
    this.journeeService.saveJournee(this.journee).subscribe(() => {
      this.chargerJournee();
    });
    this.formHebergement.controls['hebergement'].setValue("");
    this.toggleHebergement(this.journee);
  }


  /* !!=========================!!
     !!   Concernant la photo   !!
     !!=========================!! */

  //Afficher la barre d'enregistrement de la photo
  togglePhoto(journee: any) {
    console.log("toggle" + JSON.stringify(journee));
    this.divPhoto = document.getElementsByName("divPhoto");
    console.log("idjournee" + journee);
    //console.log("idjournee" + JSON.stringify(this.journee.idJournee));
    if (this.divPhoto.item(journee.indexJournee).getAttribute("data-name") === journee.idJournee.toString()) {
      console.log("entree if");
      console.log(this.divPhoto.item(journee.indexJournee).hidden);
      this.divPhoto.item(journee.indexJournee).hidden = !this.divPhoto.item(journee.indexJournee).hidden;
      console.log(this.divPhoto.item(journee.indexJournee).hidden);
    }
  }

  //ouvre le formulaire photo
  editJourneePhoto(journee: any, togglePhoto) {
    this.journee = journee;
    console.log("editjournee" + JSON.stringify(this.journee));
    console.log("journee" + JSON.stringify(journee));
    console.log("journee voyage" + JSON.stringify(this.journee.voyage));
    togglePhoto(journee);
  }

  //enregistrer la photo
  savePhoto() {
    //console.log("journee" + JSON.stringify(this.journee));
    this.journee.nomPhotoJournee = this.formPhoto.controls['nom'].value;
    this.journee.cheminPhoto = this.formPhoto.controls['chemin'].value;
    this.journee.voyage = this.voyage;
    console.log("savePhoto" + JSON.stringify(this.journee));
    this.journeeService.saveJournee2(this.journee).subscribe(() => {
      this.chargerJournee();
    });
    //console.log(JSON.stringify("activitées :" + this.activites));
    //console.log(JSON.stringify("idJournee" + this.journee.idJournee));
    this.formPhoto.controls['nom'].setValue("");
    this.formPhoto.controls['chemin'].setValue("");
    this.togglePhoto(this.journee);
  }


  /* !!===========================!!
     !!   Concernant l'activité   !!
     !!===========================!! */


  //Afficher la barre d'enregistrement de l'activité
  toggleActivite(journee: any) {
    console.log("toggle" + JSON.stringify(journee));
    this.divActivite = document.getElementsByName("divActivite");
    console.log("idjournee" + journee.toString());
    //console.log("idjournee" + JSON.stringify(this.journee.idJournee));
    if (this.divActivite.item(journee.indexJournee).getAttribute("data-name") === journee.idJournee.toString()) {
      console.log("entree if");
      console.log(this.divActivite.item(journee.indexJournee).hidden);
      this.divActivite.item(journee.indexJournee).hidden = !this.divActivite.item(journee.indexJournee).hidden;
      console.log(this.divActivite.item(journee.indexJournee).hidden);
    }
  }

  //ouvre le formulaire activité
  editJourneeActivite(journee: any, toggleActivite) {
    this.journee = journee;
    console.log("editjournee" + JSON.stringify(journee));
    console.log("journee" + JSON.stringify(this.journee));
    console.log("journee voyage" + JSON.stringify(this.journee.voyage));
    toggleActivite(journee);
  }

  //enregistrer l'activité
  saveActivite(ngOnInit) {
    console.log("journee" + JSON.stringify(this.journee));
    if (this.formActivite.controls['idActivite'].value !== null) {
      this.activite.idActivite = this.formActivite.controls['idActivite'].value;
    }
    this.activite.descriptif = this.formActivite.controls['descriptif'].value;
    this.activite.nomPhotoActivite = this.formActivite.controls['nomPhotoActivite'].value;
    this.activite.cheminPhoto = this.formActivite.controls['photo'].value;
    this.activite.journee = this.journee;
    console.log("journee activite" + JSON.stringify(this.activite.journee));
    this.activiteService.saveActivite(this.activite).subscribe(() => {
      this.chargerActivite();
    });
    console.log(JSON.stringify("activitées :" + this.activites));
    console.log(JSON.stringify("idJournee" + this.journee.idJournee));
    console.log("photo " + this.formActivite.controls["photo"].value);
    this.formActivite.controls['idActivite'].setValue("");
    this.formActivite.controls['descriptif'].setValue("");
    this.formActivite.controls['nomPhotoActivite'].setValue("");
    this.formActivite.controls['photo'].setValue("");
    this.toggleActivite(this.journee);
    this.ngOnInit();
  }

  //afficher les activités
  chargerActivite() {
    this.activiteService.getActivitesByJournee(this.journee.idJournee).subscribe(data => {
      this.activites = data
      console.log("charger Activité");
    });
  }
  //ouvre le formulaire activité
  editJourneeActivite2(activiteBack: any, journee: any, toggleActivite) {
    console.log("edit activite : " + JSON.stringify(activiteBack));
    this.journee = journee;
    this.formActivite.controls['idActivite'].setValue(activiteBack.idActivite)
    this.formActivite.controls['descriptif'].setValue(activiteBack.descriptif)
    this.formActivite.controls['nomPhotoActivite'].setValue(activiteBack.photo.nom)
    this.formActivite.controls['photo'].setValue(activiteBack.photo.chemin)
    console.log("editjournee" + JSON.stringify(this.journee));
    toggleActivite(this.journee);
  }

  //ouvrir le modal qui demande confirmation avant la suppression d'une activité
  openModalActivite(activite) {
    document.getElementById("modalSuppressionActivite").style.display = "block";
    this.activite = activite;
    console.log("idActivite : " + JSON.stringify(this.activite.idActivite));
  }

  //fermer le modal
  closeFormActivite() {
    document.getElementById("modalSuppressionActivite").style.display = "none";
  }

  //supprimer l'activité
  deleteActivite(ngOnInit) {
    this.activiteService.deleteActivite(this.activite.idActivite).subscribe(() => {
      this.chargerActivite();
      console.log("idActivite : " + JSON.stringify(this.activite.idActivite));
      this.ngOnInit();
    });
    document.getElementById("modalSuppressionActivite").style.display = "none";
  }












}

