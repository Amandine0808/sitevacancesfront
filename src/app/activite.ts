
import { Journee } from "./journee";

export class Activite {
    idActivite : number ;
    descriptif : String ;
    cheminPhoto : String ;
    nomPhotoActivite : String ;
    journee : Journee ;
}
