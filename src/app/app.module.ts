import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AlertModule } from '../../node_modules/ngx-bootstrap/alert';
import { BsDropdownModule } from '../../node_modules/ngx-bootstrap/dropdown';
import { VoyagesComponent } from './voyages/voyages.component';
import { JourneesComponent } from './journees/journees.component';
import { ActivitesComponent } from './activites/activites.component';
import { TestComponent } from './test/test.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AccueilComponent } from './accueil/accueil.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { VariableGlobale } from './variable-globale';

@NgModule({
  declarations: [
    AppComponent,
    VoyagesComponent,
    JourneesComponent,
    ActivitesComponent,
    TestComponent,
    NavbarComponent,
    AccueilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    /* pour faire fonctionner les FormGroup */
    FormsModule,
    ReactiveFormsModule,
    /* pour communiquer avec le serveur */
    HttpClientModule,
    AlertModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
  exports: [
  ],
  providers: [VariableGlobale],
  bootstrap: [AppComponent]
})
export class AppModule { }

