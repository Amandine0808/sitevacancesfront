import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActiviteBack } from './activite-back';
import { Photo } from './photo';
@Injectable({
  providedIn: 'root'
})

export class ActiviteService {
  activiteBack : ActiviteBack = new ActiviteBack ;

  constructor(private httpCLient: HttpClient) { }

  private baseUrl = "http://localhost:9090/activites";

  public getAllActivites() : Observable<any>{
    return this.httpCLient.get(this.baseUrl);
  }

  public saveActivite(activite : any): Observable<any>{
    this.activiteBack.photo = new Photo ;
    this.activiteBack.idActivite = activite.idActivite ;
    this.activiteBack.descriptif = activite.descriptif ;
    this.activiteBack.journee = activite.journee ;
    this.activiteBack.photo.nom = activite.nomPhotoActivite ;
    this.activiteBack.photo.chemin = activite.cheminPhoto ;
    return this.httpCLient.post(this.baseUrl, this.activiteBack);
  }

  public deleteActivite(id : number): Observable<any>{
    return this.httpCLient.delete(this.baseUrl+"/"+id);
  }

  public getActivite(id : number): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/"+id) ; 
  }

  public getActivitesByJournee(id : number): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/journee"+id) ; 
  }
}

