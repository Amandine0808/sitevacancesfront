

import { Journee } from "./journee";
import { Photo } from './photo';

export class Voyage {
    idVoyage : number ; 
    plusieursJours : boolean ;
    dateDebut : Date ;
    dateFin : Date ;
    lieu : String ; 
    nomPhotoVoyage : String ;
    cheminPhoto : String ; 
    journees : Array<Journee> ; 
}

