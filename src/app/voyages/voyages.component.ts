import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Voyage } from '../voyage';
import { Journee } from '../journee';
import { VoyageService } from '../voyage.service';
import { JourneeService } from '../journee.service';
import { VoyageBack } from '../voyage-back';
import { VariableGlobale } from '../variable-globale';

@Component({
  selector: 'app-voyages',
  templateUrl: './voyages.component.html',
  styleUrls: ['./voyages.component.css']
})
export class VoyagesComponent implements OnInit {
  voyages: any[];
  voyage: Voyage = new Voyage;
  selectedLink: String;
  journee: Journee = new Journee;
  journees: any[] = [0];
  formVoyage: any;
  HTMLElement : any;
  e:any;
  document : HTMLElement;
  //variableG : VariableGlobale;

  constructor(private voyageService: VoyageService, private router: Router, private journeeService: JourneeService, private variableGlobale: VariableGlobale) { }

  ngOnInit(): void {
    //déclaration des différents champs du formulaire
    this.formVoyage = new FormGroup({
      idVoyage: new FormControl(),
      plusieursJours: new FormControl(),
      dateDebut: new FormControl(),
      dateFin: new FormControl(),
      lieu: new FormControl(),
      nomPhotoVoyage: new FormControl(),
      cheminPhoto : new FormControl(),
    });
    this.chargerVoyage();
  }

  //récupérer l'ensemble des voyages
  chargerVoyage() {
    this.voyageService.getAllVoyages().subscribe(data => {
      this.voyages = data
    });
  console.log("charger voyage");
  }

  //changer la valeur de selectedLink en fonction de la case cochée
  afficherDateForm(coche: String) {
    this.selectedLink = coche;
  }

  //afficher l'un ou l'autre des formulaires date en fonction de la valeur de selectedLink
  isSelected(name: string): boolean {
    if (!this.selectedLink) {
      return false;
    }
    else {
      return (this.selectedLink === name);
    }
  }

  //enregistrer un nouveau voyage puis enregistrer la ou les journées correspondantes
  saveVoyage(saveJournee) {
    console.log("save voyage");
    this.voyageService.saveVoyage(this.formVoyage.value).subscribe(() => {
      this.chargerVoyage();
      saveJournee(this.saveSimpleJournee.bind(this));
    });
    console.log("path photo : " + JSON.stringify(this.formVoyage.controls['cheminPhoto'].value));
    this.formVoyage.controls['idVoyage'].setValue("")
    this.formVoyage.controls['plusieursJours'].setValue("")
    this.formVoyage.controls['dateDebut'].setValue("")
    this.formVoyage.controls['dateFin'].setValue("")
    this.formVoyage.controls['lieu'].setValue("")
    this.formVoyage.controls['nomPhotoVoyage'].setValue("") 
    this.formVoyage.controls['cheminPhoto'].setValue("") 
  }

  //action d'ajouter une journée
  saveSimpleJournee(){
    this.journeeService.saveJournee(this.journee).subscribe(() => {
    })
  }

  //enregistrer une ou plusieurs nouvelles journées en fonction du type de voyage
  saveJournee(saveSimpleJournee) {
    console.log("lastvoyage" + JSON.stringify(this.voyageService.getLastVoyage()));
    this.voyageService.getLastVoyage().subscribe(data => {
      this.voyage = data;
      console.log("data" + JSON.stringify(data));
      console.log("voyage" + JSON.stringify(this.voyage));
      console.log("selectedLink" + this.selectedLink);
      //si le voyage a duré un seul jour : ne créer qu'une journée
      if (this.selectedLink === "false") {
        console.log("entrée if");
        this.journee.dateJournee = this.voyage.dateDebut;
        this.journee.indexJournee = 0;
        this.journee.voyage = this.voyage;
        console.log("voyage" + JSON.stringify(this.voyage.idVoyage));
        console.log("journee" + JSON.stringify(this.journee));
        saveSimpleJournee();
        localStorage.setItem("voyageG", JSON.stringify(this.voyage));
         this.router.navigate(['/formJournees']);
        this.formVoyage.controls['plusieursJours'].setValue("");
        this.formVoyage.controls['dateDebut'].setValue("");
        this.formVoyage.controls['dateFin'].setValue("");
        this.formVoyage.controls['lieu'].setValue("");
        this.formVoyage.controls['nomPhotoVoyage'].setValue("");
        this.formVoyage.controls['cheminPhoto'].setValue(""); 
        
      }
      //si le voyage a duré plusieurs jours : créer autant de nouvelles journées
      else {
        this.journee.indexJournee = 0;
           for (var d = new Date(this.voyage.dateDebut); d <= new Date(this.voyage.dateFin); d.setDate(d.getDate() + 1)) {
             console.log("entrée for");
             this.journee.dateJournee = d;
             this.journee.voyage = this.voyage;
            saveSimpleJournee();
             this.journee.indexJournee ++;
           }
           localStorage.setItem("voyageG", JSON.stringify(this.voyage));
           this.router.navigate(['/formJournees']);
          }
        })
      };

//ouvrir le modal qui demande confirmation avant la suppression supprimer un voyage
openModal(voyage) {
      document.getElementById("modalSuppression").style.display = "block";
      this.voyage = voyage ; 
      console.log("idVoyage : " + JSON.stringify(this.voyage.idVoyage));
  }
//fermer le modal
  closeForm() {
      document.getElementById("modalSuppression").style.display = "none";
      }

//supprimer le voyage
  deleteVoyage(){
  this.voyageService.deleteVoyage(this.voyage.idVoyage).subscribe(() => {
      this.chargerVoyage();
      console.log("idVoyage : " + JSON.stringify(this.voyage.idVoyage));
    });
    document.getElementById("modalSuppression").style.display = "none";
}

  //modifier un voyage
  editVoyage(voyageBack: any) {
    console.log("voyage v " + voyageBack);
    this.openModalModification();
    this.formVoyage.controls['idVoyage'].setValue(voyageBack.idVoyage)
    this.formVoyage.controls['plusieursJours'].setValue(voyageBack.plusieursJours)
    this.formVoyage.controls['dateDebut'].setValue(voyageBack.dateDebut)
    this.formVoyage.controls['dateFin'].setValue(voyageBack.dateFin)
    this.formVoyage.controls['lieu'].setValue(voyageBack.lieu)
    this.formVoyage.controls['nomPhotoVoyage'].setValue(voyageBack.photo.nom) 
    this.formVoyage.controls['cheminPhoto'].setValue(voyageBack.photo.chemin)
    console.log("idVoyage update: " + JSON.stringify(voyageBack.idVoyage));
  }

  //enregistre les modifications du voyage
  modifierVoyage(){
    console.log("save voyage");
    this.voyageService.saveVoyage(this.formVoyage.value).subscribe(() => {
      this.chargerVoyage();
    });
    console.log("path photo : " + JSON.stringify(this.formVoyage.controls['cheminPhoto'].value));
    this.formVoyage.controls['idVoyage'].setValue("")
    this.formVoyage.controls['plusieursJours'].setValue("")
    this.formVoyage.controls['dateDebut'].setValue("")
    this.formVoyage.controls['dateFin'].setValue("")
    this.formVoyage.controls['lieu'].setValue("")
    this.formVoyage.controls['nomPhotoVoyage'].setValue("") 
    this.formVoyage.controls['cheminPhoto'].setValue("") 
    this.closeFormModification();
  }

  //aller à la page journées correspondantes au voyage
   changerPage(voyage: any){
    localStorage.setItem("voyageG", JSON.stringify(voyage));
    console.log("variable globale: " + JSON.stringify(localStorage.getItem('voyageG')));
    this.router.navigate(['/formJournees']);
  } 

  //ouvrir le modal pour modifier un voyage
openModalModification() {
  console.log("ouvrir modal");
  document.getElementById("modalModification").style.display = "block";
}
//fermer le modal
closeFormModification() {
  document.getElementById("modalModification").style.display = "none";
  }

//ouvrir le modal pour créer un voyage
openModalAjout() {
  console.log("ouvrir modal");
  document.getElementById("modalAjout").style.display = "block";
}
//fermer le modal
closeFormAjout() {
  document.getElementById("modalAjout").style.display = "none";
  }

}
