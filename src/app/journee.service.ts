import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JourneeBack } from './journee-back';
import { Photo } from './photo';
@Injectable({
  providedIn: 'root'
})


export class JourneeService {
  journeeBack : JourneeBack = new JourneeBack ;

  constructor(private httpCLient: HttpClient) { }
    
  private baseUrl = "http://localhost:9090/journees";

  public getAllJournees() : Observable<any>{
    return this.httpCLient.get(this.baseUrl);
  }

  public saveJournee3(journee : any, voyage : any, index : number): Observable<any>{
    console.log("date" +JSON.stringify(journee.dateJournee));
    console.log("hébergement"+JSON.stringify(journee.hebergement));
    journee.voyage = voyage ;
    journee.indexJournee = index ;
    return this.httpCLient.post(this.baseUrl, journee);
  }

  public saveJournee2(journee : any): Observable<any>{
    this.journeeBack.photo = new Photo ;
    this.journeeBack.idJournee = journee.idJournee ;
    this.journeeBack.dateJournee = journee.dateJournee ;
    this.journeeBack.hebergement = journee.hebergement ;
    this.journeeBack.indexJournee = journee.indexJournee ;
    this.journeeBack.photo.nom = journee.nomPhotoJournee ;
    this.journeeBack.photo.chemin = journee.cheminPhoto ;
    this.journeeBack.voyage = journee.voyage ;
    this.journeeBack.activites = journee.activites ;
    console.log("journeeBack : " + JSON.stringify(this.journeeBack));
    return this.httpCLient.post(this.baseUrl, this.journeeBack);
  }

  public saveJournee(journee : any): Observable<any>{
    return this.httpCLient.post(this.baseUrl, journee);
  }

  public deleteJournee(id : number): Observable<any>{
    return this.httpCLient.delete(this.baseUrl+"/"+id);
  }

  public getJournee(id : number): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/"+id) ; 
  }

  public getJourneeOrdreChrono(): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/ordreChrono") ; 
  }

  public getLastJournee(): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/idMax") ; 
  }

  public getJourneeByVoyage(id : number): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/voyage"+id) ; 
  }

  public getLastJourneeByVoyage(id : any): Observable<any>{
    return this.httpCLient.get(this.baseUrl+"/voyage"+id+"/idMax") ; 
  }
}

