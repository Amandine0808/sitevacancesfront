import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { ActivitesComponent } from './activites/activites.component';
import { AppComponent } from './app.component';
import { JourneesComponent } from './journees/journees.component';
import { VoyagesComponent } from './voyages/voyages.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'accueil', component: AccueilComponent },
  { path: 'formVoyages', component: VoyagesComponent },
  { path: 'formJournees', component: JourneesComponent },
  { path: 'formActivites', component: ActivitesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const ComponentsRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'accueil',
				component: AccueilComponent,
				data: {
					title: '',
				}
      },
			{
				path: 'formVoyages',
				component: VoyagesComponent,
				data: {
					title: '',
				}
      },
      {
				path: 'formJournees',
				component: JourneesComponent,
				data: {
					title: '',
				}
      },
      {
				path: 'formActivites',
				component: ActivitesComponent,
				data: {
					title: '',
				}
			}
		]
	}
];

