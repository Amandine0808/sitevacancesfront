import { Journee } from './journee';
import { Photo } from './photo';

export class VoyageBack {
    idVoyage : number ; 
    plusieursJours : boolean ;
    dateDebut : Date ;
    dateFin : Date ;
    lieu : String ; 
    photo : Photo ;
    journees : Array<Journee> ; 
}
